import json
import asyncio

import aiohttp

API_KEY = ''  # Your API key


async def get_json(client, url):
    async with client.get(url) as response:
        assert response.status == 200
        return await response.read()


async def get_weather_cities(lat, lon, client):
    data1 = await get_json(client,
                           f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API_KEY}')

    j = json.loads(data1.decode('utf-8'))
    cityname = j['name']
    save_to_file(j, cityname.lower().replace(' ', '_'))


def save_to_file(j, cityname):
    filename = f'{cityname}.json'
    pretty = json.dumps(j, indent=4, separators=(',', ': '), sort_keys=True)
    with open(filename, "w") as f:
        f.write(str(pretty))


async def main():
    async with aiohttp.ClientSession(loop=loop) as client:
        await asyncio.wait([
            get_weather_cities('55.751244', '37.618423', client),  # Moscow
            get_weather_cities('32.715736', '-117.161087', client),  # San Diego
            get_weather_cities('31.22222', '121.45806', client)  # Shanghai
        ])


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
