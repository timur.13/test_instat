from typing import List


class TicTacToeState:
    def who_win(self, mark):
        return f"Win {mark}"

    def state(self, board: List[str]):
        # считаем Х и О
        count_x, count_o = 0, 0
        for row in board:
            count_x += row.count('X')
            count_o += row.count('O')

        if not (count_x == count_o or count_x - count_o == 1):
            return False
        # Проверка условие выигрыша
        win_x, win_o = False, False

        def win(mark):
            for row in board:
                if row == mark * 3:
                    return True

            for c in range(3):
                col = [board[r][c] for r in range(3)]
                col = ''.join(col)
                if col == mark * 3:
                    return True
            d1 = [board[i][i] for i in range(3)]
            d2 = [board[i][2 - i] for i in range(3)]
            if d1 == [mark] * 3 or d2 == [mark] * 3:
                return True
            return False

        if win('X'):
            win_x = True
        if win('O'):
            win_o = True

        if win_x and win_o:
            return False
        if win_x:
            if count_o < count_x:
                return self.who_win('X')
            return False
        if win_o:
            if count_o == count_x:
                return self.who_win('O')
            return False

        return True
